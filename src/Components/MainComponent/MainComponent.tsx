import {
    Box,
    Button,
    Card,
    CardContent,
    CardMedia,
    FormControl,
    Grid,
    InputLabel,
    List,
    ListItem,
    ListItemText,
    MenuItem,
    Select,
    TextField,
    Typography,
} from "@material-ui/core";
import React, { useState } from "react";
import HighLowGame from "../../libs/high-low";

enum SortBy {
    AmountLeft,
    ShortestDistance,
}

const MainComponent = () => {
    // eslint-disable-next-line
    const [game, _setGame] = useState(new HighLowGame());
    const [sortByValue, setSortByValue] = useState(SortBy.AmountLeft);
    const [newCardValue, setNewCardValue] = useState("");
    const getSorted = () => {
        switch (sortByValue) {
            case SortBy.AmountLeft:
                return game.getCardsByAmount();
            case SortBy.ShortestDistance:
                return game.getCardsByShortestDistance();
        }
    };
    const removeCard = () => {
        game.removeCard(newCardValue);
        setNewCardValue("");
    };
    return (
        <Box>
            <Grid container spacing={3}>
                <Grid item xs={12}>
                    <Typography variant="h4">High-Low</Typography>
                </Grid>
                <Grid item xs={12}>
                    <FormControl fullWidth variant="filled">
                        <InputLabel>Sort By</InputLabel>
                        <Select
                            value={sortByValue}
                            onChange={(e) => {
                                setSortByValue(e.target.value as SortBy);
                            }}
                        >
                            <MenuItem value={SortBy.AmountLeft}>
                                Amount Left
                            </MenuItem>
                            <MenuItem value={SortBy.ShortestDistance}>
                                Shortest Distance
                            </MenuItem>
                        </Select>
                    </FormControl>
                </Grid>
                <Grid item xs={8}>
                    <TextField
                        fullWidth
                        label="New Card"
                        variant="filled"
                        value={newCardValue}
                        onChange={(e) => setNewCardValue(() => e.target.value)}
                    />
                </Grid>
                <Grid item xs={4}>
                    <Button
                        onClick={() => removeCard()}
                        variant="outlined"
                        fullWidth
                        style={{ height: "100%" }}
                    >
                        Remove Card
                    </Button>
                </Grid>
                {getSorted().map((card) => (
                    <Grid item xs={4} key={card.symbol}>
                        <Card>
                            <CardMedia>
                                <Typography variant="h2">
                                    {card.symbol}
                                </Typography>
                            </CardMedia>
                            <CardContent>
                                <List dense>
                                    {Object.entries(card)
                                        .slice(1)
                                        .map(([key, val]) => (
                                            <ListItem key={key} button>
                                                <ListItemText>
                                                    <p>
                                                        {key} : {val}
                                                    </p>
                                                </ListItemText>
                                            </ListItem>
                                        ))}
                                    <ListItem button>
                                        <ListItemText>
                                            <p>
                                                Probability Exact :{" "}
                                                {(
                                                    card.getProbability(game) *
                                                    100
                                                ).toFixed(2) + "%"}
                                            </p>
                                        </ListItemText>
                                    </ListItem>
                                    <ListItem button>
                                        <ListItemText>
                                            <p>
                                                Distance :{" "}
                                                {card
                                                    .getDistanceFromOthers(game)
                                                    .toFixed(2)}
                                            </p>
                                        </ListItemText>
                                    </ListItem>
                                </List>
                            </CardContent>
                        </Card>
                    </Grid>
                ))}
            </Grid>
        </Box>
    );
};

export default MainComponent;
