interface Deck {
    [key: string]: Card;
}

class Card {
    symbol: string;
    amount: number;
    numericalEquivlent: number;
    constructor(symbol: string, amount: number, numericalEquivlent: number) {
        this.symbol = symbol;
        this.amount = amount;
        this.numericalEquivlent = numericalEquivlent;
    }

    getProbability(game: HighLowGame): number {
        return this.amount / game.totalAmountOfCards;
    }

    getDistanceFromOthers(game: HighLowGame): number {
        let sum = 0;
        let types = 0;
        for (const card of Object.values(game.cardsInDeck)) {
            const distance = Math.abs(
                card.numericalEquivlent - this.numericalEquivlent
            );
            sum += distance * card.amount;
            types++;
        }
        return sum / types;
    }
}

export default class HighLowGame {
    cardsInDeck: Deck = {};
    constructor() {
        this.initDeck();
    }

    get totalAmountOfCards(): number {
        return Object.values(this.cardsInDeck)
            .map((card) => card.amount)
            .reduce((a, b) => a + b, 0);
    }

    initDeck() {
        this.cardsInDeck["A"] = new Card("A", 4, 1);
        for (let i = 2; i <= 10; i++) {
            this.cardsInDeck[`${i}`] = new Card(`${i}`, 4, i);
        }
        for (const [i, cardType] of ["J", "Q", "K"].entries()) {
            this.cardsInDeck[`${cardType}`] = new Card(
                `${cardType}`,
                4,
                11 + i
            );
        }
        this.cardsInDeck[`JOKER`] = new Card("JOKER", 2, 14);
    }

    removeCard(cardType: string) {
        cardType = cardType.toLocaleUpperCase();
        if (cardType in this.cardsInDeck) {
            if (this.cardsInDeck[cardType].amount > 0) {
                this.cardsInDeck[cardType].amount -= 1;
            }
        }
    }

    getCardsByAmount() {
        return Object.values(this.cardsInDeck).sort(
            (a, b) => b.amount - a.amount
        );
    }

    getCardsByShortestDistance() {
        return Object.values(this.cardsInDeck).sort(
            (a, b) =>
                a.getDistanceFromOthers(this) - b.getDistanceFromOthers(this)
        );
    }
}
