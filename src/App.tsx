import { Container, createTheme, ThemeProvider } from "@material-ui/core";
import React from "react";
import "./App.css";
import MainComponent from "./Components/MainComponent/MainComponent";

function App() {
    const theme = createTheme({
        palette: {
            type: "dark",
        },
    });
    return (
        <div className="App">
            <ThemeProvider theme={theme}>
                <Container maxWidth="md" className="MainContainer">
                    <MainComponent />
                </Container>
            </ThemeProvider>
        </div>
    );
}

export default App;
